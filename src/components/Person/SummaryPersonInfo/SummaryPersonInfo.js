import React from 'react';
import PersonInfo from "../PersonInfo/PersonInfo";
import axiosApi from "../../../axiosApi";
import {useHistory} from "react-router-dom/cjs/react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deletePerson, setModalOpen} from "../../../store/action";

const SummaryPersonInfo = () => {
    const dispatch = useDispatch();
    const editablePerson = useSelector(state => state.editablePerson);

    const history = useHistory();
    const editPersonHandler = () => {
        dispatch(setModalOpen(false));
        history.push('/edit-contact');
    };

    const deletePersonHandler = async () => {
        try {
            dispatch(setModalOpen(false));
            dispatch(deletePerson(editablePerson.personKey));
            await axiosApi.delete(`/contacts/${editablePerson.personKey}.json`);
        } catch (error) {
            console.log('Error happened while deleting person');
        }
    };

    return (
        <>
            {
                (editablePerson) &&
                <PersonInfo personContact={editablePerson.person}>
                    <p className="person-title">{editablePerson.person.phone}</p>
                    <p className="person-title">{editablePerson.person.email}</p>
                </PersonInfo>
            }
            <button onClick={editPersonHandler}>Edit</button>
            <button onClick={deletePersonHandler}>Delete</button>
        </>
    );
};

export default SummaryPersonInfo;