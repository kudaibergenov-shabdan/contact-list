import React from 'react';
import {DEFAULT_USER_IMAGE} from "../../../assets/constants";

const PersonInfo = ({children, personContact}) => {
    return (
        <div className="Person">
            <div className="person-image-block">
                <img
                    src={(personContact.photo) ? personContact.photo : DEFAULT_USER_IMAGE}
                    alt="Person"
                    width="100px"
                    height="auto"
                />
            </div>
            <div className="person-info-block">
                <p className="person-title">{personContact.name}</p>
                {children}
            </div>
        </div>
    );
};

export default PersonInfo;