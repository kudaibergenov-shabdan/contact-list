import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchContacts, setModalOpen} from "../../store/action";
import './Contacts.css';
import Person from "../Person/Person";
import Modal from "../UI/Modal/Modal";
import SummaryPersonInfo from "../Person/SummaryPersonInfo/SummaryPersonInfo";

const Contacts = () => {
    const dispatch = useDispatch();
    const contacts = useSelector(state => state.contacts);
    const isModalOpen = useSelector(state => state.isModalOpen);

    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch]);

    const closeModal = () => {
        dispatch(setModalOpen(false));
    };

    return (
        <>
            <Modal show={isModalOpen} close={closeModal}>
                <SummaryPersonInfo/>
            </Modal>
            <div className="Contacts">
                {Object.keys(contacts).map(key => (
                    <Person
                        key={key}
                        personContact={contacts[key]}
                        personKey={key}
                    />
                ))}
            </div>
        </>
    )
        ;
};

export default Contacts;