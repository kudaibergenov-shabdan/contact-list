import React from 'react';
import Header from "../Header/Header";
import Main from "../Main/Main";

const Layout = props => {
    return (
        <>
            <Header />
            <Main children={props.children}/>
        </>
    );
};

export default Layout;