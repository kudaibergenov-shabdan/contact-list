import axiosApi from "../axiosApi";

export const USERS_FETCH_SUCCESS = 'USERS_FETCH_SUCCESS';
export const USERS_FETCH_FAILURE = 'USERS_FETCH_FAILURE';
export const OPEN_MODAL = 'OPEN_MODAL';
export const EDIT_PERSON = 'EDIT_PERSON';
export const EDIT_PERSON_ATTRIBUTE = 'EDIT_PERSON_ATTRIBUTE';
export const DELETE_PERSON = 'DELETE_PERSON';

const fetchContactsSuccessAction = contacts => ({type: USERS_FETCH_SUCCESS, payload: contacts});
const fetchContactsFailureAction = () => ({type: USERS_FETCH_FAILURE});
const openModal = isOpen => ({type: OPEN_MODAL, payload: isOpen});
const editPersonHandler = personKey => ({type: EDIT_PERSON, payload: personKey})
export const editPersonAttribute = (personAttribute) => ({type: EDIT_PERSON_ATTRIBUTE, payload: personAttribute})
export const deletePerson = personKey => ({type: DELETE_PERSON, payload: personKey})

export const fetchContacts = () => {
    return async dispatch => {
        try {
            const contacts = await axiosApi.get('/contacts.json');
            dispatch(fetchContactsSuccessAction(contacts.data));
        } catch (error) {
            dispatch(fetchContactsFailureAction());
        }
    }
};

export const setModalOpen = isOpen => {
    return openModal(isOpen);
};

export const editPerson = personKey => {
    return async dispatch => {
        const person = await axiosApi.get(`/contacts/${personKey}.json`);
        const personToEdit = {personKey, person: person.data};
        dispatch(editPersonHandler(personToEdit));
    }
};