import {
    DELETE_PERSON,
    EDIT_PERSON,
    EDIT_PERSON_ATTRIBUTE,
    OPEN_MODAL,
    USERS_FETCH_FAILURE,
    USERS_FETCH_SUCCESS
} from "./action";

const initialState = {
    contacts: {},
    isModalOpen: false,
    editablePerson: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case USERS_FETCH_SUCCESS:
            return {
                ...state,
                contacts: action.payload
            };
        case USERS_FETCH_FAILURE:
            return state;
        case OPEN_MODAL:
            return {
                ...state,
                isModalOpen: action.payload
            };
        case EDIT_PERSON:
            return {
                ...state,
                editablePerson: action.payload
            }
        case EDIT_PERSON_ATTRIBUTE:
            const attributeName = action.payload['attributeName'];
            const attributeValue = action.payload['attributeValue'];
            return {
                ...state,
                editablePerson:
                    {
                        ...state.editablePerson,
                        person:
                            {
                                ...state.editablePerson.person,
                                [attributeName]: attributeValue
                            }
                    }
            };
        case DELETE_PERSON:
            const {[action.payload]: deletedKey, ...newContacts} = state.contacts;
            return {
                ...state,
                editablePerson: null,
                contacts: newContacts
            }
        default:
            return state;
    }
};

export default reducer;