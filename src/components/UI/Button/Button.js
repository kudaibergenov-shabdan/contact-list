import React from 'react';
import './Button.css';

const Button = props => {
  return (
    <button
      onClick={props.onClick}
      className={['Button', props.type].join(' ')}
      type={props.buttonType}
    >
      {props.children}
    </button>
  );
};

export default Button;