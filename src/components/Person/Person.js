import React from 'react';
import './Person.css';
import {useDispatch} from "react-redux";
import {editPerson, setModalOpen} from "../../store/action";
import PersonInfo from "./PersonInfo/PersonInfo";

const Person = ({personContact, personKey}) => {
    const dispatch = useDispatch();

    const updatePerson = () => {
        dispatch(editPerson(personKey));
        dispatch(setModalOpen(true));
    }

    return (
        <PersonInfo personContact={personContact}>
            <div className="person-title">
                <button onClick={updatePerson}>Update</button>
            </div>
        </PersonInfo>
    );
};

export default Person;