import React from 'react';
import './ContactData.css';
import Button from "../UI/Button/Button";

const ContactData = props => {
    const goBack = async () => {
        props.history.replace('/');
    };

    let form = (
        <form onSubmit={props.submitContact}>
            <input
                className="Input"
                type="text"
                name="name"
                placeholder="Name"
                value={props.user.name}
                onChange={props.onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="phone"
                placeholder="Phone"
                value={props.user.phone}
                onChange={props.onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="email"
                placeholder="email"
                value={props.user.email}
                onChange={props.onInputChange}
            />
            <input
                className="Input"
                type="text"
                name="photo"
                placeholder="Url of photo"
                value={props.user.photo}
                onChange={props.onImageChange}
            />
            <div>
                <img
                    src={props.userImage}
                    alt="Contact"
                    width="100px"
                    height="auto"
                />
            </div>
            <div>
                {props.children}
                <Button onClick={goBack} buttonType="button" type="Info">BACK TO CONTACTS</Button>
            </div>
        </form>
    );

    return (
        <div className="ContactData">
            <h4>{props.pageName}</h4>
            {form}
        </div>
    );
};

export default ContactData;