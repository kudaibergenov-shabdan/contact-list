import axios from "axios";

const axiosApi = axios.create({
    baseURL: 'https://burder-js-default-rtdb.firebaseio.com'
})

export default axiosApi;