import React, {useState} from 'react';
import ContactData from "../ContactData/ContactData";
import Button from "../UI/Button/Button";
import axiosApi from "../../axiosApi";
import {DEFAULT_USER_IMAGE} from "../../assets/constants";
import axios from "axios";

const AddContact = props => {
    const [userImage, setUserImage] = useState(DEFAULT_USER_IMAGE);
    const [user, setUser] = useState({
        name: '',
        phone: '',
        email: '',
        photo: ''
    });

    const onInputChange = e => {
        const {name, value} = e.target;
        setUser(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const onImageChange = async e => {
        onInputChange(e);
        const photoUrl = e.target.value;
        try {
            await axios.get(photoUrl);
            setUserImage(photoUrl);
        } catch (error) {
            setUserImage(DEFAULT_USER_IMAGE);
        }
    };

    const submitContact = async e => {
        e.preventDefault();
        try {
            await axiosApi.post('/contacts.json', user);
            props.history.replace('/');
        } catch (error) {
            console.log('Error happened while creating contact:', error);
        }
    };

    return (
        <div>
            <ContactData
                pageName="Add new contact"
                user={user}
                userImage={userImage}
                onInputChange={onInputChange}
                onImageChange={onImageChange}
                submitContact={submitContact}
            >
                <Button
                    onClick={submitContact}
                    buttonType="button"
                    type="Success"
                >
                    CREATE
                </Button>
            </ContactData>
        </div>
    );
};

export default AddContact;