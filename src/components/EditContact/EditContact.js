import React from 'react';
import ContactData from "../ContactData/ContactData";
import Button from "../UI/Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {editPersonAttribute} from "../../store/action";
import {useHistory} from "react-router-dom/cjs/react-router-dom";
import axiosApi from "../../axiosApi";

const EditContact = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const editablePerson = useSelector(state => state.editablePerson);

    const onInputChange = e => {
        const {name, value} = e.target;
        dispatch(editPersonAttribute({attributeName: name, attributeValue:value}));
    };

    const onImageChange = e => {
        onInputChange(e);
    };

    const submitContact = async e => {
        e.preventDefault();
        try {
            await axiosApi.put(`/contacts/${editablePerson.personKey}.json`, editablePerson.person);
            history.replace('/');
        } catch (error) {
            console.log('Error happened while updating contact:', error);
        }
    };

    return (
        <>
            {
                (editablePerson) &&
                <ContactData
                    pageName="Edit contact"
                    user={editablePerson.person}
                    userImage={editablePerson.person.photo}
                    onInputChange={onInputChange}
                    onImageChange={onImageChange}
                    submitContact={submitContact}
                >
                    <Button
                        onClick={submitContact}
                        buttonType="button"
                        type="Success"
                    >
                        UPDATE
                    </Button>
                </ContactData>
            }
        </>
    );
};

export default EditContact;