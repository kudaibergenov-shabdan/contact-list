import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header className="Header">
            <h1 className="Header__title">Contact List</h1>
            <nav className="Header__main-nav main-nav">
                <ul className="main-nav__list">
                    <li className="main-nav__list-item">
                        <NavLink className="main-nav__list-item-link" exact to="/">Home</NavLink>
                    </li>
                    <li className="main-nav__list-item">
                        <NavLink className="main-nav__list-item-link" to="/add-contact">Add contact</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default Header;