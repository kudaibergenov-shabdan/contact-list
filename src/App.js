import {Switch, Route} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Home from "./containers/Home/Home";
import AddContact from "./components/AddContact/AddContact";
import EditContact from "./components/EditContact/EditContact";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/add-contact" exact component={AddContact} />
            <Route path="/edit-contact" exact component={EditContact} />
            <Route render={() => <h1>Not found</h1>} />
        </Switch>
    </Layout>
);

export default App;
